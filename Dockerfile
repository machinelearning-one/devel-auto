# -------------------------------
# machinelearning-one/devel-auto
# -------------------------------

# Set the base image
# -------------------
FROM ubuntu:20.04

# Define required arguments
# --------------------------
ARG AUTHOR
ARG EMAIL
ARG USERNAME
ARG PASSWORD

# Define commands for headless installation
# ------------------------------------------
ENV DEBIAN_FRONTEND=noninteractive \
    APT_INSTALL="apt install -y --no-install-recommends" \
    PIP_INSTALL="python -m pip --no-cache-dir install --upgrade"

# Setup the locale
# -----------------
RUN apt update && \
    $APT_INSTALL locales && \
    locale-gen en_US.UTF-8 && \
    echo LANG=en_US.UTF-8 > /etc/default/locale

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# Install basic utilities
# ------------------------
RUN apt update && \
    $APT_INSTALL \
    sudo \
    build-essential \
    cmake \
    apt-utils \
    apt-transport-https \
    software-properties-common \
    ca-certificates \
    wget \
    curl \
    git \
    vim \
    libssl-dev \
    openssh-client \
    unzip \
    unrar

# Install python 3.8 and pip
# ---------------------------
ENV PATH=$PATH:~/.local/bin

RUN apt update && \
    $APT_INSTALL \
    python3.8 \
    python3.8-dev \
    python3.8-distutils \
    && \
    wget -O ~/get-pip.py \
    https://bootstrap.pypa.io/get-pip.py && \
    python3.8 ~/get-pip.py && \
    ln -s /usr/bin/python3.8 /usr/local/bin/python

# Install numfocus and allied packages
# -------------------------------------
RUN $PIP_INSTALL \
    numpy \
    scipy \
    pandas \
    scikit-image \
    scikit-learn \
    matplotlib \
    seaborn 

# Install pytorch and allied packages
# ------------------------------------
RUN $PIP_INSTALL \
    torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113 && \
    $PIP_INSTALL \
    einops \
    pytorch-lightning \
    torchmetrics \
    hydra-core \
    hydra-colorlog \
    hydra-optuna-sweeper

# Install quality of life packages
# ---------------------------------
RUN $PIP_INSTALL \
    Cython \
    typing \
    pre-commit \
    black[jupyter] \
    flake8 \
    isort \
    nbstripout \
    python-dotenv \
    tqdm \
    rich \
    pytest \
    sh \
    pudb \
    twine

# Install jupyterlab
# -------------------
RUN $PIP_INSTALL \
    jupyterlab \
    ipywidgets

# Install opencv
# ---------------
RUN apt update && \
    $APT_INSTALL \
    libopencv-dev python3-opencv

# Install docker 
# ---------------
RUN apt update && \
    $APT_INSTALL \
    gpg-agent && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" && \
    $APT_INSTALL \
    docker-ce-cli

# Install dagger
# ---------------
RUN cd /usr/local && \
    curl -L https://dl.dagger.io/dagger/install.sh | sh

# Install Rust
# -------------
ARG RUSTUP_VERSION=1.24.3
ARG RUST_VERSION=1.59.0

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=$PATH:/usr/local/cargo/bin

RUN RUST_ARCH=x86_64-unknown-linux-gnu && \
    url="https://static.rust-lang.org/rustup/archive/${RUSTUP_VERSION}/${RUST_ARCH}/rustup-init" && \
    wget "${url}" && \
    chmod +x rustup-init && \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION --default-host $RUST_ARCH && \
    rm rustup-init && \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOME

# Install Go
# -----------
ARG GO_VERSION=1.18

ENV PATH=$PATH:/usr/local/go/bin

RUN curl -OL https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz && \
    rm go${GO_VERSION}.linux-amd64.tar.gz

# Install Deno
# -------------
ARG DENO_VERSION=1.20.1

RUN curl -fsSL https://github.com/denoland/deno/releases/download/v${DENO_VERSION}/deno-x86_64-unknown-linux-gnu.zip \
    --output deno.zip \
    && unzip deno.zip -d /usr/local/bin/ \
    && chmod 755 /usr/local/bin/deno \
    && rm deno.zip

# Install NVM and Node, enable alternative package managers
# ----------------------------------------------------------
ARG NVM_VERSION=v0.39.1
ARG NODE_VERSION=16.14.2

ENV NVM_DIR=/usr/local/nvm \
    NODE_PATH=$NVM_DIR/v${NODE_VERSION}/lib/node_modules \
    PATH=$PATH:$NVM_DIR/versions/node/v${NODE_VERSION}/bin

RUN mkdir -p $NVM_DIR && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/${NVM_VERSION}/install.sh | sh && \
    . ${NVM_DIR}/nvm.sh && \
    . ${NVM_DIR}/bash_completion && \
    echo ". ${NVM_DIR}/nvm.sh" >> /etc/bash.bashrc && \
    echo ". ${NVM_DIR}/bash_completion" >> /etc/bash.bashrc && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default && \
    corepack enable

# Install upx
# ------------
RUN apt update && \
    $APT_INSTALL upx

# Add autoenv
# ------------
RUN echo "autoenv() { if [ -x .autoenv ]; then source .autoenv ; echo '.autoenv executed' ; fi } ;" >> /etc/bash.bashrc && \
    echo "cd() { builtin cd \"\$@\" ; autoenv ; } ; autoenv" >> /etc/bash.bashrc

# Perform cleanup
# ---------------
RUN ldconfig && \
    apt clean && \
    apt autoremove && \
    rm -rf -- /var/lib/apt/lists/* /tmp/* ~/*

# Create and configure the user
# ------------------------------
RUN useradd -m -s /bin/bash -p $(openssl passwd -1 $PASSWORD) $USERNAME && \
    # Add the user to the sudo group
    # -------------------------------
    usermod -aG sudo $USERNAME && \
    # Configure git
    # --------------
    git config --global user.name "$AUTHOR" && \
    git config --global user.email "$EMAIL"

# Set the default user and working directory
# -------------------------------------------
USER $USERNAME
WORKDIR /home/$USERNAME
